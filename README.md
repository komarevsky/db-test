db-test
=======

---
db-test project uses Apache v2 license.

---
**db-test** provides you with class *com.freebetbot.dbtest.DbTest*.

*DbTest* is *JUnit4* test case which does the following:
* creates HSQL Server instance in constructor
* runs HSQL Server instance before (*@Before*) each test
* shutdowns HSQL Server instance after (*@After*) each test

---
**Before usage:**

The project is not published in any maven repository, so for usage you must downdload it and install in your local repository as follows:

        git clone https://github.com/komarevsky/db-test.git
        cd db-test
        mvn install


---
**Usage:**
* add dependency in *pom.xml* :

        <dependency>
            <groupId>com.freebetbot.db-test</groupId>
            <artifactId>db-test</artifactId>
            <version>1.0.0-SNAPSHOT</version>
            <scope>test</scope>
        </dependency>

* create test which requires *HSQLDB*. Example:

        package com.freebetbot.dbtest.independent;
    
        import com.freebetbot.dbtest.DbTest;
        import org.junit.Before;
        import org.junit.Test;
    
        public class IndependentTest extends DbTest {
    
            @Before
            public void setUp() {
                System.out.println("create required table structure");
                // example assumes existence of file: src/test/resources/create_test_tables.sql
                //executeSqlFile("src/test/resources/create_test_tables.sql");
            }
        
            @Test
            public void queryTest() {
                executeSqlQuery("create table mytable(data int);");
                executeSqlQuery("insert into mytable values(23); commit;");
                // ... do something with data
            }
        
            @Test
            public void yetAnotherTest() {
                // example assumes existence of file: src/test/resources/fill_test_tables.sql
                //executeSqlFile("src/test/resources/fill_test_tables.sql");
                // ... do something with data
            }
        }
    
* Inside of test code you can connect to HSQLDB in the way you like. Here is example of connection settings for *Hibernate* :

        <properties>
            ...
            <property name="connection.driver_class">org.hsqldb.jdbc.JDBCDriver</property>
            <property name="connection.url">jdbc:hsqldb:hsql://localhost:12001/testdb</property>
            <property name="hibernate.connection.autocommit">true</property>
            <property name="connection.username">sa</property>
            <property name="connection.password"></property>
            ...
        </properties>

---
**Features:**
* default database name is **testdb**, but can be changed via constructor
* default server port is **12001**, but can be changed via constructor
* server stores data in memory. After test is finished, server is shutdown and all the data is lost.
* server is created with user **sa** and without password
* autocommit mode set to *false*. Include *"...; commit;"* in insert/update statements. Add *"set autocommit true;"* to sql files
