package com.freebetbot.dbtest;

import org.junit.After;
import org.junit.Before;

public class DbTest {

    protected HsqldbServer hsqldbServer;
    protected SqlToolWrapper sqlToolWrapper;

    public DbTest(final String dbName, final int port) {
        hsqldbServer = new HsqldbServer(dbName, port);
        sqlToolWrapper = new SqlToolWrapper(dbName, port);
    }

    public DbTest(final String dbName) {
        this(dbName, HsqlConstant.DEFAULT_PORT);
    }

    public DbTest(final int port) {
        this(HsqlConstant.DEFAULT_DB_NAME, port);
    }

    public DbTest() {
        this(HsqlConstant.DEFAULT_DB_NAME, HsqlConstant.DEFAULT_PORT);
    }

    public String getDbName() {
        return hsqldbServer.getDbName();
    }

    public int getPort() {
        return hsqldbServer.getPort();
    }

    @Before
    public void setUpDbTestUniqueName() {
        hsqldbServer.startServer();
    }

    @After
    public void tearDownDbTestUniqueName() {
        hsqldbServer.shutdownServer();
    }

    public void executeSqlFile(String sqlFile) {
        sqlToolWrapper.executeSqlFile(sqlFile);
    }

    public void executeSqlQuery(String sqlQuery) {
        sqlToolWrapper.executeSqlQuery(sqlQuery);
    }


}
