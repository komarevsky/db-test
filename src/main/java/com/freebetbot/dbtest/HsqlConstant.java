package com.freebetbot.dbtest;

public class HsqlConstant {

    public static final String DEFAULT_DB_NAME = "testdb";
    public static final int DEFAULT_PORT = 12001;

    private HsqlConstant() {}
}
