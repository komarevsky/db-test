package com.freebetbot.dbtest;

import org.apache.log4j.Logger;
import org.hsqldb.persist.HsqlProperties;
import org.hsqldb.server.Server;
import org.junit.Assert;

class HsqldbServer {

    private static final Logger LOGGER = Logger.getLogger(HsqldbServer.class);

    protected Server hsqlServer;
    protected HsqlProperties hsqlProperties;
    protected String dbName;
    protected int port;

    public HsqldbServer(final String dbName, final int port) {
        hsqlServer = new Server();
        hsqlProperties = new HsqlProperties();

        this.dbName = dbName;
        this.port = port;

        configureServer();
    }

    public String getDbName() {
        return dbName;
    }

    public int getPort() {
        return port;
    }
    
    public void startServer() {
        hsqlServer.start();
    }

    public void shutdownServer() {
        hsqlServer.shutdownWithCatalogs(0);
    }

    protected void configureServer() {
        hsqlProperties.setProperty("server.port", port);
        hsqlProperties.setProperty("server.database.0", "mem:" + dbName);
        hsqlProperties.setProperty("server.dbname.0", dbName);
        hsqlProperties.setProperty("server.silent", true);
        hsqlProperties.setProperty("server.trace", false);
        try {
            hsqlServer.setProperties(hsqlProperties);
        } catch (Exception ex) {
            String msg = "properties for server are not loaded";
            LOGGER.error(msg, ex);
            Assert.fail(msg);
        }

        hsqlServer.setRestartOnShutdown(false);
        hsqlServer.setNoSystemExit(true);
        hsqlServer.setLogWriter(null);
    }

}
