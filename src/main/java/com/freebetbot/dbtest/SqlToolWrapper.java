package com.freebetbot.dbtest;

import org.apache.log4j.Logger;
import org.hsqldb.cmdline.SqlTool;
import org.junit.Assert;

import java.util.Arrays;

class SqlToolWrapper {

    private static final Logger LOGGER = Logger.getLogger(SqlToolWrapper.class);

    private String inlineArg;

    public SqlToolWrapper(final String dbName, final int port) {
        inlineArg = new StringBuilder("--inlineRc=url=jdbc:hsqldb:hsql://localhost:")
                .append(port)
                .append("/")
                .append(dbName)
                .append(",user=sa,password=")
                .toString();
    }

    public void executeSqlFile(String sqlFile) {
        runSqlTool(new String[] {inlineArg, sqlFile});
    }

    public void executeSqlQuery(String sqlQuery) {
        runSqlTool(new String[] {inlineArg, "--sql", sqlQuery});
    }

    protected void runSqlTool(String[] args) {
        try {
            SqlTool.objectMain(args);
        } catch (SqlTool.SqlToolException ex) {
            String msg = "sql tool failed with the args:" + Arrays.toString(args);
            LOGGER.error(msg, ex);
            Assert.fail(msg);
        }
    }

}
