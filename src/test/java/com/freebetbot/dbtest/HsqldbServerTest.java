package com.freebetbot.dbtest;

import org.junit.Test;

public class HsqldbServerTest {

    @Test
    public void startStopTest() {
        HsqldbServer server = new HsqldbServer(HsqlConstant.DEFAULT_DB_NAME, HsqlConstant.DEFAULT_PORT);
        server.startServer();
        server.shutdownServer();
        server.startServer();
        server.shutdownServer();
        // if we here then no exception was thrown
    }
}
