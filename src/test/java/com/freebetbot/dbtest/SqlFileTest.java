package com.freebetbot.dbtest;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

public class SqlFileTest extends DbTest {

    private static final Logger LOGGER = Logger.getLogger(SqlFileTest.class);

    @Before
    public void setUp() {
        LOGGER.debug("setUp called");
        executeSqlFile("src/test/resources/create_test_tables.sql");
        LOGGER.debug("setUp finish");
    }

    @Test
    public void stubTest() {
        // if we here, then setUp passed
        LOGGER.debug("test passed");
    }

    @Test
    public void sqlFileExecutionTest() {
        executeSqlFile("src/test/resources/fill_test_tables.sql");
    }
}
