package com.freebetbot.dbtest;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

public class SqlQueryTest extends DbTest {

    private static final Logger LOGGER = Logger.getLogger(SqlQueryTest.class);

    @Before
    public void setUp() {
        executeSqlQuery("create table something(data int);");
    }

    @Test
    public void stubQueryTest() {
        // if we here, then setUp passed
        LOGGER.debug("test passed");
    }

    @Test
    public void runQueryTest() {
        executeSqlQuery("create table house(data int);");
    }

}
