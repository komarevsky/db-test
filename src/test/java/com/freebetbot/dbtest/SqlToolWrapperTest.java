package com.freebetbot.dbtest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SqlToolWrapperTest {

    private HsqldbServer hsqldbServer;
    private SqlToolWrapper sqlToolWrapper;

    @Before
    public void setUp() {
        hsqldbServer = new HsqldbServer(HsqlConstant.DEFAULT_DB_NAME, HsqlConstant.DEFAULT_PORT);
        hsqldbServer.startServer();
        sqlToolWrapper = new SqlToolWrapper(hsqldbServer.getDbName(), hsqldbServer.getPort());
    }

    @After
    public void tearDown() {
        hsqldbServer.shutdownServer();
    }

    @Test
    public void testExecuteSqlFile() {
        sqlToolWrapper.executeSqlFile("src/test/resources/create_test_tables.sql");
        sqlToolWrapper.executeSqlFile("src/test/resources/fill_test_tables.sql");
        // if we here then no exception was thrown
    }

    @Test
    public void testExecuteSqlQuery() {
        sqlToolWrapper.executeSqlQuery("create table something(data int);");
        // if we here then no exception was thrown
    }
}
