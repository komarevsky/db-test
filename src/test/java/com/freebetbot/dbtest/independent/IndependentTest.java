package com.freebetbot.dbtest.independent;

import com.freebetbot.dbtest.DbTest;
import org.junit.Before;
import org.junit.Test;

public class IndependentTest extends DbTest {

    @Before
    public void setUp() {
        System.out.println("create required table structure");
        // example assumes existence of file: src/test/resources/create_test_tables.sql
        //executeSqlFile("src/test/resources/create_test_tables.sql");
    }

    @Test
    public void queryTest() {
        executeSqlQuery("create table mytable(data int);");
        executeSqlQuery("insert into mytable values(23); commit;");
        // ... do something with data
    }

    @Test
    public void yetAnotherTest() {
        // example assumes existence of file: src/test/resources/fill_test_tables.sql
        //executeSqlFile("src/test/resources/fill_test_tables.sql");
        // ... do something with data
    }
}
