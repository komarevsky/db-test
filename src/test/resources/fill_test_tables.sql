set autocommit true;

insert into transport_type(tt_name) values('BUS');
insert into transport_type(tt_name) values('TRAM');
insert into transport_type(tt_name) values('TROLLEY_BUS');
insert into transport_type(tt_name) values('METRO');

insert into line(l_name, l_direction, l_transport_type_fk)
select '18', 'station1 - station2', tt_id
from transport_type
where tt_name='BUS';

insert into line(l_name, l_direction, l_transport_type_fk)
select '37', 'station1 - station3', tt_id
from transport_type
where tt_name='TRAM';
